
# Note: Repository not in use anymore

The work in this repository has been superseded by work in the following repos:
[https://bitbucket.org/terndatateam/eem-frontend/src/master/](https://bitbucket.org/terndatateam/eem-frontend/src/master/) and [https://bitbucket.org/terndatateam/eemetlqueries/src/master/](https://bitbucket.org/terndatateam/eemetlqueries/src/master/). Henceforth, please do not use this repository. Currently this repository is kept as it is for historical reasons, however, in the future it may be deleted completely.

----


Note: This document is targeted at developers working on the project.



This is a HTTP RESTful API built using Sails.js. In development it runs directly on your machine using NodeJS and connects to any Postgres instance (Docker-ised or not). When deployed, the Sails app runs inside AWS Lambda and connects to a cloud-hosted database.

This project is built using CircleCI and uses continuous deployment for production and a staging environment. Errors are reported to Rollbar and we implement API versioning using the `Accept` HTTP header thanks to the [sails-hook-api-version-accept](https://github.com/tomsaleeba/sails-hook-api-version-accept) Sails hook.

## Requirements
  1. NodeJS >= v8.10
  1. [yarn](http://yarnpkg.io) >= 1.7.0
  1. (optionaly) Docker >= 18.03

## Quickstart (running with a remote database)
  1. Make sure you have all the dependencies installed:

        yarn
        
  1. We assume that the remote database is already populated so you just need to set the URL and lift sails:

        export PG_URL='postgresql://user:password@host.example.com:5432/dbname'
        node app.js


## Quickstart (running with a local database)
  1. You can run a local Postgres using Docker with:

        docker run \
          -e POSTGRES_USER=docker \
          -e POSTGRES_PASSWORD=docker \
          -e POSTGRES_DB=docker \
          -p 5432:5432 \
          --detach \
          --name=eem-pg \
          postgres:10


  1. Then, you need some data for the API to serve up. You have two options here:
      - use the [eem-loader](https://bitbucket.org/terndatateam/eem-loader) project to populate your database
      - restore a database dump

  1. Now, start the API:

        sails lift

  1. Later, you can clean up by stopping and removing the Postgres container with:

        docker rm -f eem-pg


## Deployment
This project is a plain SailsJS app that you can run/develop locally but it's designed to run in (pre)production on AWS Lambda. Deployment happens as part of the CircleCI build. Read more in [./.circleci/README.md](./.circleci/README.md)

When deploying the database for staging/production, you might have to fix table ownership issues, you can do that with:

  1. connect to the Postgres DB
  
        psql -h <host> -U <user> -p <port> eem-api-staging # or -prod

  1. generate some queries to fix the ownership issues
  
        select 'alter table ' || tablename || ' owner to staging;'
        from pg_tables
        where tableowner = 'squid';
      
  1. copy the output, and run it

## Swagger documentation
We have a Sails hook to generate a Swagger definition at `/swagger.json` but it's currently disabled until it supports all the various route definitions we use.

## Architecture
The techinical requirements for this project are:

  1. can run on a local workstation, for development. Also avoids vendor lock-in
  1. quick start up time, ideally < 1 second, to keep AWS Lambda cold start wait times down
  1. can run on AWS Lambda, for easy+reliable production operation
  1. make writing a RESTful HTTP API, backed by a DB, easy
  1. integrates with databases that work for us

We've chosen SailsJS to satisfy these requirements. It runs locally and using Apex UP we can deploy it to AWS Lambda. Sails offers Blueprint routes so we get most of the endpoints we need right out-of-the-box.

### Vocabulary model choices
We've chosen to represent each vocabulary, models starting with `Lut*`, as their own model. They have the same schema but it's easiest to keep them separate so each has semantic meaning.

The `Lut` prefix stands for *Look Up Table*.
