var sails = require('sails')

// Before running any tests...
before(function (done) {

  if (this.timeout() < 5000) {this.timeout(5000)}
  sails.lift({
    hooks: {
      'api-version-accept': true,
      'swagger-generator': false,
      grunt: false,
      views: false,
      session: false,
    },
    log: {
      level: 'warn' // change to warn, debug, etc when troubleshooting tests
    },
    models: {
      migrate: 'drop',
      attributes: {
        createdAt: { type: 'number', autoCreatedAt: true, },
        updatedAt: { type: 'number', autoUpdatedAt: true, },
        id: {
          type: 'number',
          autoMigrations: { autoIncrement: true },
          columnName: 'id', // override our config/model.js value so sails-disk doesn't complain when doing .update()
        },
      }
    },
    datastores: {
      default: {
        adapter: 'sails-disk',
        url: null,
        inMemoryOnly: true,
      }
    },
  },(err) => {
    if (err) {return done(err)}
    return done()
  })
})

// After all tests have finished...
after((done) => {
  sails.lower(done)
})
