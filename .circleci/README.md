# Getting set up with CircleCI
You need to configure a few things in CircleCI for this CI/CD pipeline to work.

## Environment variables
 1. [AWS credentials](https://circleci.com/docs/2.0/deployment-integrations/#aws) (`AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`) for an account that has the access required by [Apex UP](https://up.docs.apex.sh/#aws_credentials.iam_policy_for_up_cli) to do the deployment
 1. `STAGING_PG_URL` as the URL for the Postgres staging DB
 1. `PRODUCTION_PG_URL` as the URL for the Postgres production DB
 1. `ROLLBAR_ACCESS_TOKEN` for notifying [Rollbar](https://docs.rollbar.com/reference#section-authentication) of a deployment

## Deployment workflow
Doing the *actual* code deployment is easy, just push a commit to the `staging` or `master` branches. CircleCI will build and deploy for you.

Of course, deploying the code is only half of the solution. You also need to deploy some data as this API is read-only. There are two options for deploying data:

 1. restore a database dump to your target DB. The user that the API connects as only *needs* `select` permissions, so you probably can't use that user to do the restore. Once you've restored the DB, make sure the user that the API connects as has the permissions to `select`. It'll explode when you hit an API endpoint if you haven't done this.
 1. use the [eem-loader](https://bitbucket.org/terndatateam/eem-loader) project to populate your database of choice.
