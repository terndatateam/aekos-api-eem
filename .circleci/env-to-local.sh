#!/usr/bin/env sh
# Converts environment variables to Sails local config.
# We're doing this because we don't have a Pro license
# for Apex UP, which you need for env support.
set -e
cd `dirname "$0"`
cd ..
: ${PG_URL:?}
: ${AWS_ACCESS_KEY_ID:?}
: ${AWS_SECRET_ACCESS_KEY:?}
: ${ROLLBAR_ENV:?}

target=config/local.js

echo "
module.exports = {
  datastores: {
    default: {
      url: '$PG_URL'
    },
  },
  custom: {
    swaggerJsonPath: '/tmp/swagger.json',
    rollbarEnv: '$ROLLBAR_ENV',
    rollbarAccessToken: '$ROLLBAR_ACCESS_TOKEN',
  },
  log: {
    level: 'warn',
  },
}
" > $target
