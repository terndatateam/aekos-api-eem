// intended to be require()d by the eem-loader
const VError = require('verror')

module.exports = function lift (configOverrides) {
  return new Promise((resolve, reject) => {
    process.chdir(__dirname)
    let sails
    try {
      const SailsApp = require('sails').Sails
      sails = new SailsApp()
      const rc = require('sails/accessible/rc')
      const sailsrcConfig = rc() // FIXME *needs* all hooks in the node_modules dir
      configOverrides.hooks = sailsrcConfig.hooks
    } catch (err) {
      return reject(new VError(err, 'Failed to create new Sails app'))
    }
    sails.lift(configOverrides, (err, sails) => {
      if (err) {
        return reject(new VError(err, 'Failed to lift sails'))
      }
      return resolve(sails)
    })
  })
}
