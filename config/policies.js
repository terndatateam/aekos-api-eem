/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/
  // '*': true,
  // Policy: Check all request verb and ONLY allow GET to go through
  // This will be applied to all Blueprint buildin actions derived from models
  // see: /api/policies/validateRequest.js for implementation of this policy
  '*': 'validateRequest'
};
