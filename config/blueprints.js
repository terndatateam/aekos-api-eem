/**
 * Blueprint API Configuration
 * (sails.config.blueprints)
 *
 * For background on the blueprint API in Sails, check out:
 * https://sailsjs.com/docs/reference/blueprint-api
 *
 * For details and more available options, see:
 * https://sailsjs.com/config/blueprints
 */

module.exports.blueprints = {

  /***************************************************************************
  *                                                                          *
  * Automatically expose implicit routes for every action in your app?       *
  *                                                                          *
  ***************************************************************************/

  actions: true,


  /***************************************************************************
  *                                                                          *
  * Automatically expose RESTful routes for your models?                     *
  *                                                                          *
  ***************************************************************************/

  rest: true, //mosheh. false kills stuff like /model?fieldName=xyz, i.e. blueprint autoroutes based on defined models for api end points die - Do NOT set to false if you want the framework to expose models as API end points

  /***************************************************************************
  *                                                                          *
  * Automatically expose CRUD "shortcut" routes to GET requests?             *
  * (These are enabled by default in development only.)                      *
  *                                                                          *
  ***************************************************************************/

  shortcuts: false, // mosheh. false kills stuff like /:model/find/:id, /:model/create, /:model/destroy/:id. Make true in dev ONLY! Prod definately false
  parseBlueprintOptions: function(req) {

    // Get the default query options.
    var queryOptions = req._sails.hooks.blueprints.parseBlueprintOptions(req);
  
    // If this is the "find" or "populate" blueprint action, and the normal query options
    // indicate that the request is attempting to set an exceedingly high `limit` clause,
    // then prevent it (we'll say `limit` must not exceed 1000).
    if (req.options.blueprintAction === 'find' || req.options.blueprintAction === 'populate') {
      if (queryOptions.criteria.limit > 1000) {
        queryOptions.criteria.limit = 1000;
      }
    }

    // console.log(`req.options = ${JSON.stringify(req.options, null, 2)}`)
    // console.log(`queryOptions = ${JSON.stringify(queryOptions, null, 2)}`)
  
    return queryOptions;
  
  }
};
