/**
 * Custom configuration
 * (sails.config.custom)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */

module.exports.custom = {

  swaggerJsonPath: './swagger/swagger.json', // relative to project root
  rollbarEnv: 'dev',
  rollbarAccessToken: false, // disabled in dev
}
