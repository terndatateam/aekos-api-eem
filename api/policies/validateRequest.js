/**
 * validateRequest.js
 * By: Mosheh EliYahu
 * Date: 30-07-2018
 * @description :: Check the request verb if its a GET. If not disallow action access
 *
 */

module.exports = async function (req, res, proceed) {

  // let notAllowedRequestTypes = ['PUT','POST', 'PATCH', 'DELETE','GET'];
  let allowedRequestTypes = ['GET'];

  let httpVerb = req.method;
  if (allowedRequestTypes.includes(httpVerb)) {
    return proceed();
  }
  return res.forbidden();
};
