/**
 * EemController
 * Note: This controller action will be removed later since we are going to use the Blueprint's default api from models
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  friendlyName: 'Get EEM data for Sampling Unit',

  description: 'Builds data in the vegetation exchange format',

  inputs: {
    // TODO allow filters
    pageSize: {
      type: 'number',
      min: 1,
      max: 10000,
      defaultsTo: 1000,
    }
  },

  exits: {
    success: {
      // doing this causes the custom response handler in
      // sails-hook-api-version-accept to be called because we
      // hit this case: https://github.com/sailshq/machine-as-action/blob/v10.1.1/lib/machine-as-action.js#L1075
      // and not this one: https://github.com/sailshq/machine-as-action/blob/v10.1.1/lib/machine-as-action.js#L761
      responseType: 'ok',
    },
  },

  fn: async function (inputs, exits) {
    const observations = await SamplingUnit
      .find({ limit: inputs.pageSize })
    return exits.success(observations)
  }

}
