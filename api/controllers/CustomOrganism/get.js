/**
 * EemController
 * Note: This controller action will be removed later since we are going to use the Blueprint's default api from models
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  friendlyName: 'Get EEM data',

  description: 'Builds data in the vegetation exchange format',

  inputs: {
    // TODO allow filters
    pageSize: {
      type: 'number',
      min: 1,
      max: 10000,
      defaultsTo: 5,
    },
    surveyId: {
      type: 'string',
      defaultsTo: '',
      required: false
    },
    property: {
      type: 'string',
      defaultsTo: '', // 'Height',
      required: false

    },
    rangeLow: {
      type: 'string', // for now
      defaultsTo: '',
      required: false
    },
    rangeHigh: {
      type: 'string', // for now
      defaultsTo: '',
      required: false
    },
  },

  exits: {
    success: {
      // doing this causes the custom response handler in
      // sails-hook-api-version-accept to be called because we
      // hit this case: https://github.com/sailshq/machine-as-action/blob/v10.1.1/lib/machine-as-action.js#L1075
      // and not this one: https://github.com/sailshq/machine-as-action/blob/v10.1.1/lib/machine-as-action.js#L761
      responseType: 'ok',
    },
  },

  fn: async function (inputs, exits) {

    var whereQry = {}

    if (inputs.surveyId !== '') {
      whereQry.surveyId = inputs.surveyId
    }
    if (inputs.property !== '') {
      whereQry.property = inputs.property
    }
    if (inputs.rangeLow !== '') {
      whereQry.rangeLow = inputs.rangeLow
    }
    if (inputs.rangeHigh !== '') {
      whereQry.rangeHigh = inputs.rangeHigh
    }

    var qryParam = {}
    if (Object.keys(whereQry).length > 0) {
      qryParam.where = whereQry
    }

    qryParam.limit = inputs.pageSize

    const observations = await Organism.find(qryParam)
    // Note:cleaning the data - however dont need to do this if db data is clean.
    // will be clean eventually!
    const cleanObservations = await observations.reduce(async (accumPromise, curr) => {
      const accum = await accumPromise
      curr.value = curr.value === null ? '' : await sails.helpers.removeDoubleQuotes(curr.value)
      curr.rangeLow = curr.rangeLow === null ? '' : await sails.helpers.removeDoubleQuotes(curr.rangeLow)
      curr.rangeHigh = curr.rangeHigh === null ? '' : await sails.helpers.removeDoubleQuotes(curr.rangeHigh)
      curr.category = curr.category === null ? '' : await sails.helpers.removeDoubleQuotes(curr.category)
      curr.comment = curr.comment === null ? '' : await sails.helpers.removeDoubleQuotes(curr.comment)
      curr.standard = curr.standard === null ? '' : await sails.helpers.removeDoubleQuotes(curr.standard)
      accum.push(curr)
      return accum
    }, Promise.resolve([]))
    // return exits.success(observations)
    return exits.success(cleanObservations)
  }
}
