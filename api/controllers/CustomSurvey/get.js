/**
 * EemController
 * Note: This action will be remove in leaue of REsful api provided by the Survey model
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  friendlyName: 'Get Survey Identifiers',

  description: 'Returns a list of survey ids in the system e.g. "aekos.org.au/collection/sa.gov.au/bdbsa_veg/survey_40"',

  inputs: {
    pageSize: {
      type: 'number',
      min: 1,
      max: 10000,
      defaultsTo: 1000,
    }
  },

  exits: {
    success: {
      // doing this causes the custom response handler in
      // sails-hook-api-version-accept to be called because we
      // hit this case: https://github.com/sailshq/machine-as-action/blob/v10.1.1/lib/machine-as-action.js#L1075
      // and not this one: https://github.com/sailshq/machine-as-action/blob/v10.1.1/lib/machine-as-action.js#L761
      responseType: 'ok',
    },
  },

  fn: async function (inputs, exits) {

    const surveys = await Metadata.find({ limit: inputs.pageSize })

    const cleanSurveys = await surveys.reduce(async (accumPromise, curr) => {
      const accum = await accumPromise
      curr.citation = curr.citation === null ? '' : await sails.helpers.removeDoubleQuotes(curr.citation)
      curr.custodian = curr.custodian === null ? '' : await sails.helpers.removeDoubleQuotes(curr.custodian)
      curr.dateAccessioned = curr.dateAccessioned === null ? '' : await sails.helpers.removeDoubleQuotes(curr.dateAccessioned)
      curr.dateModified = curr.dateModified === null ? '' : await sails.helpers.removeDoubleQuotes(curr.dateModified)
      curr.language = curr.language === null ? '' : await sails.helpers.removeDoubleQuotes(curr.language)
      curr.licence = curr.licence === null ? '' : await sails.helpers.removeDoubleQuotes(curr.licence)
      curr.methodLink = curr.methodLink === null ? '' : await sails.helpers.removeDoubleQuotes(curr.methodLink)
      curr.organisation = curr.organisation === null ? '' : await sails.helpers.removeDoubleQuotes(curr.organisation)
      curr.persistentsurveyId = curr.persistentsurveyId === null ? '' : await sails.helpers.removeDoubleQuotes(curr.persistentsurveyId)
      curr.rights = curr.rights === null ? '' : await sails.helpers.removeDoubleQuotes(curr.rights)
      curr.surveyMethod = curr.surveyMethod === null ? '' : await sails.helpers.removeDoubleQuotes(curr.surveyMethod)
      curr.suveryName = curr.suveryName === null ? '' : await sails.helpers.removeDoubleQuotes(curr.suveryName)
      curr.surveyType = curr.surveyType === null ? '' : await sails.helpers.removeDoubleQuotes(curr.surveyType)

      accum.push(curr)
      return accum
    }, Promise.resolve([]))

    return exits.success(cleanSurveys)
  }

}
