/**
 * EemController
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    friendlyName: 'Get number of records in SamplingUnit table',
  
    description: 'Returns the number of records in the Samplingunit table',
  
    inputs: {
    },
    exits: {
      success: {
        // doing this causes the custom response handler in
        // sails-hook-api-version-accept to be called because we
        // hit this case: https://github.com/sailshq/machine-as-action/blob/v10.1.1/lib/machine-as-action.js#L1075
        // and not this one: https://github.com/sailshq/machine-as-action/blob/v10.1.1/lib/machine-as-action.js#L761
        responseType: 'ok',
      },
    },
  
    fn: async function (inputs, exits) {
      var numRecords = await SamplingUnit.count();
      return exits.success(numRecords)
    }
  }
  