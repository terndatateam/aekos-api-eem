module.exports = {
  attributes: {
    surveyId: {
      type: 'string',
      allowNull: true
    },
    stateName: {
      type: 'string'
    },
    datasetName: {
      type: 'string'
    },
    surveyName: {
      type: 'string',
      allowNull: true
    },
    ultimateFeatureOfInterest: {
      type: 'string',
      allowNull: true
    },
    observationId: {
      type: 'number',
      allowNull: true
    },
    featureId: {
      type: 'string',
      allowNull: true
    },
    featureOfInterest: {
      type: 'string',
      allowNull: true
    },
    featureQualifier: {
      type: 'string',
      allowNull: true
    },
    orignalFeatureOfInterest: {
      type: 'string',
    },
    protocolLink: {
      type: 'string',
      allowNull: true
    },
    protocol: {
      type: 'string',
      allowNull: true
    },
    property: {
      type: 'string',
      allowNull: true
    },
    propertyQualifier: {
      type: 'string',
      allowNull: true
    },
    value: {
      type: 'string',
      allowNull: true
    },
    rangeLow: {
      type: 'string',
      allowNull: true
    },
    rangeHigh: {
      type: 'string',
      allowNull: true
    },
    category: {
      type: 'string',
      allowNull: true
    },
    comment: {
      type: 'string',
      allowNull: true
    },
    standard: {
      type: 'string',
      allowNull: true
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.organism',
  },
}
