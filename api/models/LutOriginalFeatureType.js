/**
 * LutOriginalFeatureType.js
 *
 * LUT = Look Up Table, as this is vocabulary
 * Original = the original term used in the source dataset, not the term used for interchange
 */
module.exports = {
  attributes: {
    name: {
      type: 'string',
    },
    description: {
      type: 'string',
    },
    link: {
      type: 'string',
    },
    parentConcept: {
      type: 'string',
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.lutoriginalfeaturetype'
  }
}
