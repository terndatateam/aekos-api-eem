/**
 * LutPropertyQualifierType.js
 *
 * LUT = Look Up Table, as this is vocabulary
 */
module.exports = {
  attributes: {
    name: {
      type: 'string',
    },
    description: {
      type: 'string',
    },
    link: {
      type: 'string',
    },
    parentConcept: {
      type: 'string',
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.lutpropertyqualifiertype'
  },
}
