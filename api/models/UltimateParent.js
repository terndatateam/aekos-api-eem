/**
 * UltimateParent.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    entity: {
      type: 'string',
    },
    parent: {
      type: 'string',
    },
    ultimateStudylocationEntityId: {
      type: 'string',
    }
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.ulimateparent',
  },
}
