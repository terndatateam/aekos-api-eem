/**
 * Observation.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    feature: {
      model: 'feature',
    },
    ultimateFeatureOfInterest: {
      model: 'feature',
    },
    propertyType: {
      model: 'lutpropertytype',
    },
    propertyQualifierType: {
      model: 'lutpropertyqualifiertype',
    },
    procedure: {
      model: 'procedure',
    },
    results: {
      collection: 'result',
      via: 'observation'
    },
    times: {
      collection: 'time',
      via: 'observation'
    },
    storageSet: {
      model: 'storageset',
    },
    sensors: {
      collection: 'sensor',
      via: 'observation'
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.observation',
  },
}
