/**
 * StorageSet.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    storageSetUri: {
      type: 'string',
      required: true
    },
    observations: {
      collection: 'observation',
      via: 'storageSet'
    },
    times: {
      collection: 'time',
      via: 'storageSet'
    },
    features: {
      collection: 'feature',
      via: 'storageSet'
    },
    sensors: {
      collection: 'sensor',
      via: 'storageSet'
    },
    results: {
      collection: 'result',
      via: 'storageSet'
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.storageset'
  },
}
