/**
 * Metadata.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    surveyEntityId: {
      type: 'string'
     },
    citation: {
      type: 'string'
    },
    custodian: {
      type: 'string'
    },
    dateAccessioned: {
      type: 'number'
    },
    dateModified: {
      type: 'number'
    },
    language: {
      type: 'string'
    },
    licence: {
      type: 'string'
    },
    methodLink: {
      type: 'string' // FIXME should this be TempMethod or Procedure?
    },
    organisation: {
      type: 'string'
    },
    persistentSurveyId: {
      type: 'string'
    },
    rights: {
      type: 'string'
    },
    surveyMethod: {
      type: 'string'
    },
    surveyName: {
      type: 'string'
    },
    surveyType: {
      type: 'string'
    },
    surveyLinks: {
      collection: 'surveylink',
      via: 'metadata'
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.metadata'
  },
}
