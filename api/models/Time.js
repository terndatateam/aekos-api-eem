/**
 * Time.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    epochMs: {
      type: 'number',
    },
    timeType: {
      type: 'string',
    },
    observation: {
      model: 'observation',
    },
    storageSet: {
      model: 'storageset'
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.time'
  },
}
