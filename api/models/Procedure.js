/**
 * Procedure.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    procedureEntityId: {
      type: 'string',
    },
    name: {
      type: 'string',
    },
    summary: {
      type: 'string',
    },
    details: {
      type: 'string',
    },
    observations: {
      collection: 'observation',
      via: 'procedure'
    }
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.procedure',
  },
}
