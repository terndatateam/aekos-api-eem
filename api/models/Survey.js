/**
 * Survey.js
 * Note: The Survey model is exactly as the Metatada model. I created the Survey model becuase more intiutive.
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'metadata',
  //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
  //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
  //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
  attributes: {

    id: {
      type: 'string',
      columnName: 'survey_id',
      autoIncrement: true,
      unique: true
    },
    citation: {
      type: 'string',
      columnName: 'citation'
    },
    custodian: {
      type: 'string',
      columnName: 'custodian'
    },
    dateAccessioned: {
      type: 'string',
      columnName: 'date_accessioned'
    },
    dateModified: {
      type: 'string',
      columnName: 'date_modified'
    },
    language: {
      type: 'string',
      columnName: 'language'
    },
    license: {
      type: 'string',
      columnName: 'licence'
    },
    methodLink: {
      type: 'string',
      columnName: 'method_link'
    },
    organisation: {
      type: 'string',
      columnName: 'organisation'
    },
    surveyId: {
      type: 'string',
      columnName: 'persistentsurvey_id'
    },
    rights: {
      type: 'string',
      columnName: 'rights'
    },
    surveyMethod: {
      type: 'string',
      columnName: 'survey_method'
    },
    suveryName: {
      type: 'string',
      columnName: 'survey_name'
    },
    surveyType: {
      type: 'string',
      columnName: 'survey_type'
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.survey'
  },
};

