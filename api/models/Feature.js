/**
 * Feature.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    id: require('../util/stringId'),
    featureType: {
      model: 'lutfeaturetype',
    },
    observations: {
      collection: 'observation',
      via: 'feature'
    },
    featureQualifierType: {
      model: 'lutfeaturequalifiertype',
    },
    featureRelationships: {
      collection: 'featurerelationship',
      via: 'primaryFeature'
    },
    storageSet: {
      model: 'storageset',
    },
    surveyLinks: {
      collection: 'surveylink',
      via: 'studyLocation'
    },
    originalFeatureType: {
      model: 'lutoriginalfeaturetype',
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.feature',
  },
}
