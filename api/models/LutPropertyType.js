/**
 * LutPropertyType.js
 *
 * LUT = Look Up Table, as this is vocabulary
 * This 'not original' version is the term in common use between groups, for interchange.
 */
module.exports = {
  attributes: {
    name: {
      type: 'string',
    },
    description: {
      type: 'string',
    },
    link: {
      type: 'string',
    },
    parentConcept: {
      type: 'string',
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.lutpropertytype',
  },
}
