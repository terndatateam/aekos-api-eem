/**
 * SamplingUnit.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    observationId: {
      type: 'number'
    },
    stateName: {
      type: 'string'
    },
    datasetName: {
      type: 'string'
    },
    sampledAreaSamplingUnitEntityId: {
      type: 'string',
    },
    studyLocationEntityId: {
      type: 'string',
    },
    custodian: {
      type: 'string',
      allowNull: true
    },
    rights: {
      type: 'string',
      allowNull: true
    },
    bibliographicReference: { // AKA citation
      type: 'string',
      allowNull: true
    },
    dateModified: {
      type: 'number',
      allowNull: true
    },
    language: {
      type: 'string',
      allowNull: true
    },
    surveyId: {
      type: 'string',
      allowNull: true
    },
    surveyName: {
      type: 'string',
      allowNull: true
    },
    surveyOrganisation: {
      type: 'string',
      allowNull: true
    },
    licence: {
      type: 'string',
      allowNull: true
    },
    surveyType: {
      type: 'string',
      allowNull: true
    },
    surveyMethodology: {
      type: 'string',
      allowNull: true
    },
    surveyMethodologyDescription: {
      type: 'string',
      allowNull: true
    },
    studyLocationId: { // Note: NOT the entity id
      type: 'string',
      allowNull: true
    },
    originalSiteCode: { // Note: last part of the studyloc id
      type: 'string',
      allowNull: true
    },
    province: {
      type: 'string',
      allowNull: true
    },
    geodeticDatum: {
      type: 'string',
      allowNull: true
    },
    latitude: {
      type: 'string',
      allowNull: true
    },
    longitude: {
      type: 'string',
      allowNull: true
    },
    locationDescription: {
      type: 'string',
      allowNull: true
    },
    aspect: {
      type: 'string',
      allowNull: true
    },
    slope: {
      type: 'string',
      allowNull: true
    },
    landformPattern: {
      type: 'string',
      allowNull: true
    },
    landformElement: {
      type: 'string',
      allowNull: true
    },
    elevation: {
      type: 'string',
      allowNull: true
    },
    visitId: {
      type: 'string',
      allowNull: true
    },
    visitDate: {
      type: 'string',
      allowNull: true
    },
    visitOrganisation: {
      type: 'string',
      allowNull: true
    },
    visitObservers: {
      type: 'string',
      allowNull: true
    },
    siteDescription: {
      type: 'string',
      allowNull: true
    },
    condition: {
      type: 'string',
      allowNull: true
    },
    structuralForm: {
      type: 'string',
      allowNull: true
    },
    ownerClassification: {
      type: 'string',
      allowNull: true
    },
    currentClassification: {
      type: 'string',
      allowNull: true
    },
    samplingUnitId: {
      type: 'string',
      allowNull: true
    },
    samplingUnitArea: {
      type: 'string',
      allowNull: true
    },
    samplingUnitShape: {
      type: 'string',
      allowNull: true
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.samplingunit'
  },
}
