/**
 * SurveyLink.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    studyLocation: {
      model: 'feature',
    },
    studyLocationSubgraphId: {
      type: 'string',
    },
    metadata: {
      model: 'metadata',
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.surveylink'
  },
}
