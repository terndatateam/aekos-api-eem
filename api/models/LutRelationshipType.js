/**
 * LutRelationshipType.js
 *
 * LUT = Look Up Table, as this is vocabulary
 */
module.exports = {
  attributes: {
    name: {
      type: 'string'
    },
    description: {
      type: 'string'
    },
  },
  versionConfig: {
    versions: ['v1'],
    vendorPrefix: 'vnd.au.org.tern.lutrelationshiptype'
  },
}
