/**
 * A hook to *configure* the sails-hook-swagger-generator, which is a hook itself.
 */

module.exports = function defineSwaggerHook(sails) {

  // We have to do this here, if we wait until .initialize(), the swagger-generator
  // hook has already loaded.
  sails.config['swagger-generator'] = {
    swaggerJsonPath: sails.config.custom.swaggerJsonPath,
    swagger: {
      swagger: '2.0',
      info: {
        title: 'TERN AEKOS HTTP API',
        description: 'RESTful API for interacting with TERN data',
        termsOfService: null, // TODO
        contact: {
          name: 'Mosheh Eliyahu',
          url: 'https://bitbucket.org/meliyahu/',
          email: null // TODO
        },
        license: {
          name: 'Apache 2.0', // TODO
          url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
        },
        version: '1.0.0'
      },
      host: 'localhost:1337',
      basePath: '/',
      externalDocs: {
        url: 'https://bitbucket.org/terndatateam/aekos-api-eem'
      }
    }
  }

  return {

    routes: {
      before: {
        'get /swagger.json': (_, res) => {
          const swaggerJson = require(sails.config.custom.swaggerJsonPath)
          if (!swaggerJson) {
            res
              .status(404)
              .set('content-type', 'application/json')
              .send({message: 'Cannot find swagger.json, has the server generated it?'})
          }
          return res
            .status(200)
            .set('content-type', 'application/json')
            .send(swaggerJson)
        }
      }
    }

  }

}
