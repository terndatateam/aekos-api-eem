module.exports = {

  friendlyName: 'Get object property value in an array of objects',


  description: 'Returns the value of a property in array objects',


  inputs: {

    objArray: {
      type: 'ref',
      example: 'array',
      description: 'An array of json objects',
      required: true
    },
    key: {
      type: 'string',
      example: 'Standard',
      description: 'The key name in the a json object in the array whose value we are looking for',
      required: true
    }

  },


  fn: async (inputs, exits) => {
    let length = inputs.objArray.length
    let arr = inputs.objArray
    let key = inputs.key
    for (var i = 0; i < length; i++) {
      if (arr[i].hasOwnProperty(key)) {
        var value = arr[i][key]
        let index = value.indexOf('^')
        if (index !== -1) {
          value = value.slice(0, index).replace(/\D/g, '')
        }
        // return exits.success(arr[i][key])
        return exits.success(value.replace(/^"|"$/g, ''))
      }
    }
    return exits.success('N/A')
  }

};
