module.exports = {


  friendlyName: 'Observation site id',


  description: 'Returns the observation site id, i.e. the Study Location id. e.g. aekos.org.au/collection/wa.gov.au/ravensthorpe/R163 and the original site id i.e. R163',


  inputs: {
    observationId: {
      type: 'number',
      example: 100,
      description: 'The id of the observation. i.e. the primary key value in the observation table',
      required: true
    }
  },


  exits: {
    success: {
      outputFriendlyName: 'Site Id object',
      outputDescription: 'The site id which has the full studylocation id and the original site id from dataset supplier',
    },

    noSiteIdFound: {
      description: 'Could not find site id for the supplied observation id.'
    }
  },


  fn: async (inputs, exits) => {

    // Find the result ids in the observation_result table
    const observationResult = await ObservationResult.find({ observationResult: inputs.observationId })
    if (observationResult === null || observationResult.length === 0) {
      throw 'noSiteIdFound'
    }

    // Now we find the values in the Result table
    // We are only interested in a row whose element column is Value and the value column's
    // value is what we are after.
    const result = await observationResult.reduce(async (accumPromise, curr) => {
      const accum = await accumPromise
      const resultModel = await Result.findOne({ id: curr.id })
      if (resultModel !== null) {
        const tempResult = {}
        tempResult[resultModel.element] = resultModel.value
        accum.push(tempResult)
      }
      return accum
    }, Promise.resolve([]))

    if (result === null || result.length === 0) {
      throw 'noSiteIdFound'
    }

    // Use the getObjectInArray helper function to get the value of the property Value
    const siteId = await sails.helpers.getObjectInArray(result, 'Value');

    // Also get the orginal site id from custodian
    // E.g. in aekos.org.au/collection/wa.gov.au/ravensthorpe/R163 we want R163
    const orgSiteId = siteId.split('/').pop();
    const siteIdObj = { siteId: siteId, originalSiteId: orgSiteId }

    return exits.success(siteIdObj);
  }
};

