module.exports = {

  friendlyName: 'Remove double quotes and the char ^ in a string',


  description: 'Return a clean string without double qoute inside',


  inputs: {

    value: {
      type: 'string',
      example: '"mosheh"',
      description: 'The string to be cleaned of double quotes',
      required: true
    }

  },

  fn: async (inputs, exits) => {
    var value = inputs.value
    if (value !== null && value !== '') {
      //value = value.replace(/^"|"$/g, '')
      value = value.replace(/"/g, '')
      let index = value.indexOf('^')
      if (index !== -1) {
        //value = value.slice(0, index).replace(/\D/g, '')
        value = value.slice(0, index)
      }
    }
    return exits.success(value)
  }

};
