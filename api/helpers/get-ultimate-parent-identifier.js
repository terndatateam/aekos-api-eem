module.exports = {


  friendlyName: 'Study location idenfier',


  description: 'Returns the Study Location id in which the entity (feature) belongs to. e.g. aekos.org.au/collection/wa.gov.au/ravensthorpe/R163 and the original site id i.e. R163',


  inputs: {
    entityId: {
      type: 'string',
      example: '<http://www.aekos.org.au/ontology/1.0.0/oeh_vis#SPECIESORGANISMGROUP-T1531457857228>',
      description: 'The feature/entity whose ultimate study location it belongs to',
      required: true
    }
  },


  exits: {
    success: {
      outputFriendlyName: 'Study location identifier, aka. ultimateFeatureOfInterest',
      outputDescription: 'Study location identifier, aka. ultimateFeatureOfInterest',
    },

    noUltimateParentFound: {
      description: 'Could not find studylocation id for the supplied feature entity id.'
    }
  },


  fn: async (inputs, exits) => {

    // Lets arm ourselves with the property type first
    const propertyType = await LutPropertyType.findOne({ name: 'Identifier' })
    if (propertyType === null) {
      throw 'noUltimateParentFound'
    }

    // Ok, first find the ultimate study location entity id
    // for the supplied feature
    // console.log('inputs.entityId = ' + inputs.entityId)
    const slEntityId = await UltimateParent.findOne({ entity: inputs.entityId })
    if (slEntityId === null) {
      throw 'noUltimateParentFound'
    }

    // Find observation id in the Observation table - very complex!!
    const observation = await Observation.findOne({ feature: slEntityId.ultimateStudylocationEntityId, propertyType: propertyType.id })
    if (observation === null) {
      throw 'noUltimateParentFound'
    }

    // Find the result ids in the observation_result table
    const observationResult = await ObservationResult.find({ observationResult: observation.id })
    if (observationResult === null || observationResult.length === 0) {
      throw 'noUltimateParentFound'
    }

    // Now we find the values in the Result table
    // We are only interested in a row whose element column is Value and the value column's
    // value is what we are after.
    const result = await observationResult.reduce(async (accumPromise, curr) => {
      const accum = await accumPromise
      const resultModel = await Result.findOne({ id: curr.id })
      if (resultModel !== null) {
        const tempResult = {}
        tempResult[resultModel.element] = resultModel.value
        accum.push(tempResult)
      }
      return accum
    }, Promise.resolve([]))

    if (result === null || result.length === 0) {
      throw 'noUlimateParentFound'
    }

    // Use the getObjectInArray helper function to get the value of the property Value
    const siteId = await sails.helpers.getObjectInArray(result, 'Value');

    // Also get the orginal site id from custodian
    // E.g. in aekos.org.au/collection/wa.gov.au/ravensthorpe/R163 we want R163
    // const orgSiteId =siteId.split("/").pop();
    // const siteIdObj = {siteId: siteId,originalSiteId:orgSiteId}
    // return exits.success(siteIdObj);
    return exits.success(siteId);
  }


};
